#!/usr/bin/python

import sys, threading
from serial import *
from time import *
from OSC import *

# GLOBALES
BAUDRATE = 9600

SEND_ADDRESS = '127.0.0.1', 8000
RECEIVE_ADDRESS = '127.0.0.1', 9000
PREFIX = "/floor"

floorboard = Serial(sys.argv[1], BAUDRATE)

def displayHandler (addr, tags, stuff, source):
    value = stuff[0]
    #print value
    byte1 = (value >> 8) & 0x03
    byte2 = value & 0xFF
    floorboard.write (chr(byte1))
    floorboard.write (chr(byte2))
    #print str(byte1) + " - " + str(byte2)

def __main():
    if len(sys.argv) < 2:
        exit("Muy mal!")
    try:
        # Se abre el puerto serie
        
        floorboard.open()

        # Se crea el cliente OSC
        client = OSCClient()
        client.connect (SEND_ADDRESS)

        # Se crea el servidor OSC
        server = OSCServer (RECEIVE_ADDRESS)
        server.addMsgHandler ("/floor/display", displayHandler)
 
        st = threading.Thread( target = server.serve_forever )
        st.start()
        
        while (1):
            
            if floorboard.inWaiting() >= 2:
                msB = ord(floorboard.read())
                lsB = ord(floorboard.read())
              
                space = (msB & 0xC0) >> 6
                meth = (msB & 0x3C) >> 2
                value = lsB | ((msB & 0x03) << 8)
                print str(meth) + " - " + str(value)
                
                # Se crea el mensaje OSC
                msg = OSCMessage()
                address = PREFIX
                if space == 1:
                    address += "/pedal/" + str(meth+1)
                    msg.setAddress(address)
                    msg.append(value/1000.0)
                if space == 0:
                    address += "/button/" + str(meth)
                    msg.setAddress(address)
                    msg.append(value)
               

                # Se envia el mensaje
                client.send(msg)

    except SerialException:
        print ("Puerto...")
    except KeyboardInterrupt:
        print ("Que te den!")
        server.close()
        st.join() ##!!!
        floorboard.close()

# FUNCION PRINCIPAL
if __name__ == '__main__':
    __main()
