

#define BTN A2
#define EXP1 A1
#define EXP2 A0
#define CLK 6
#define DATA_PIN 7

const uint8_t analogArray[3] = {
  EXP1,
  EXP2,
  BTN  
};


uint8_t numArray[10] = {
  0b00111111,
  0b00000110,
  0b01011011,
  0b01001111,
  0b01100110,
  0b01101101,
  0b01111101,
  0b00000111,
  0b01111111,
  0b01100111
};



void setup () {
  clearScreen();
  Serial.begin (9600);
  pinMode (CLK, OUTPUT);
  pinMode(DATA_PIN, OUTPUT); 
  
  TIMSK2 &= ~(1<<TOIE2);
  /* Configure timer2 in normal mode (pure counting, no PWM etc.) */  
  TCCR2A &= ~((1<<WGM21) | (1<<WGM20));  
  TCCR2B &= ~(1<<WGM22);  
  /* Select clock source: internal I/O clock */  
  ASSR &= ~(1<<AS2);  
  /* Disable Compare Match A interrupt enable (only want overflow) */  
  TIMSK2 &= ~(1<<OCIE2A);  
  /* Now configure the prescaler to CPU clock divided by 128 */  
  TCCR2B |= (1<<CS22)  | (1<<CS20); // Set bits  
  TCCR2B &= ~(1<<CS21);             // Clear bit  
  TIMSK2 |= (1<<TOIE2); 
}


void loop () {
  checkADCs();
}


ISR(TIMER2_OVF_vect) {
  sei ();
  while (Serial.available() > 1)
  {
    uint8_t byte1 = Serial.read();
    uint8_t byte2 = Serial.read();
    
    uint8_t space = (byte1 >> 4) & 0x03;
    uint8_t method = (byte1 >> 2) & 0x03;
    int value = ((byte1 & 0x03) << 8) | byte2;
    if (space == 0)
    {
      switch (method)
      {
        case 0:
          printNumber (value);
          break;
      }  
    }
    
  }
}


void clearScreen () {
  shiftOut (DATA_PIN, CLK, MSBFIRST, 0x00);
  shiftOut (DATA_PIN, CLK, MSBFIRST, 0x00);
  shiftOut (DATA_PIN, CLK, MSBFIRST, 0x00);
}

void sendData(uint8_t space, uint8_t method, int value) {
  Serial.write((space << 6) | ((method << 2) & 0x3C) | ((value >> 8) & 0x03));
  Serial.write(value & 0xFF);
}


int current[3]; 
int previous[3];
uint8_t currentButton = 0, previousButton = 0;
uint8_t tolerance = 7;

void checkADCs() {
    
  for (uint8_t adcCounter = 0; adcCounter < 2; adcCounter++)
  {
    current[adcCounter] = constrain (map (analogRead(analogArray[adcCounter]), 750, 1000, 0, 1000)/5.0 + 4.0*current[adcCounter]/5.0, 0, 1000);
    if (abs(previous[adcCounter]-current[adcCounter]) > tolerance)
      {
        previous[adcCounter] = current[adcCounter];
        sendData(1, adcCounter,current[adcCounter]);
      }
  }
  
  current[2] = analogRead(analogArray[2]);
  if (button (current[2]) != 11)
    currentButton = button (current[2]);
  if (currentButton != previousButton) {
    if (previousButton != 0)    // Si habia algun boton pulsado...
      sendData(0, button (previous[2]), LOW);
    if (currentButton != 0)
      sendData(0, button (current[2]), HIGH);
    previousButton = currentButton;
  }
}

uint8_t button (int adcValue)
{
  if (adcValue == 0) return 2;
  else if (adcValue >= 240  && adcValue <= 241) return 1;
  else if (adcValue >= 410  && adcValue <= 414) return 9;
  else if (adcValue >= 542  && adcValue <= 546) return 5;
  else if (adcValue >= 649  && adcValue <= 651) return 6;
  else if (adcValue >= 729  && adcValue <= 735) return 7;
  else if (adcValue >= 799  && adcValue <= 800) return 8;
  else if (adcValue >= 920  && adcValue <= 924) return 10;
  else if (adcValue >= 943  && adcValue <= 946) return 4;
  else if (adcValue >= 962  && adcValue <= 967) return 3;
  else if (adcValue > 990) return 0;
  else return 11;      // not valid
}

void printNumber (int n) {
  uint8_t n1 = n%10;                  // LSB
  uint8_t n2 = ((n-n1)%100)/10;
  uint8_t n3 = ((n-n2)%1000)/100;     // MSB
  
  int last;
  if (n > 999)
    last = numArray[n3] | 1<<7;
  else last = numArray[n3];
  shiftOut (DATA_PIN, CLK, MSBFIRST, numArray[n1]);
  shiftOut (DATA_PIN, CLK, MSBFIRST, numArray[n2]);
  shiftOut (DATA_PIN, CLK, MSBFIRST, last);
}
